import React from 'react'

class Step1 extends React.Component{

    render(){
       

         if(this.props.currentStep !== 1){
            return null; 
        }
        
        return(
            <div className="row">
                <div className="form-group col-6">
                    <label htmlFor="firstName">Firstname</label>
                    <input 
                    type="text" 
                        className="form-control" 
                        id="firstName" 
                        name="firstName"
                        placeholder="Enter firstname" 
                        value={this.props.firstName} 
                        onChange={this.props.handleChange('firstName')}/>
                </div>
                <div className="form-group col-6">
                    <label htmlFor="lastName">Lastname</label>
                    <input 
                    type="text" 
                    className="form-control" 
                    id="lastName" 
                    placeholder="Enter lastname" 
                    value={this.props.lastName} 
                    onChange={this.props.handleChange('lastName')}/>
                    
                </div>
                <div className="form-group col-6">
                    <label htmlFor="dob" className="col-2 col-form-label">Date of birth</label>
                    <input 
                    className="form-control" 
                    type="date" value={this.props.dob} 
                    id="dob" onChange={this.props.handleChange('dob')}/>
                </div>
                <button id="subbutton"className="btn btn-success btn-block">Sign up</button>
                <small id="subbutton" className="form-text text-muted">We'll always share your details with the highest bidder.</small>
            </div>
        )
    }
}

export default Step1;