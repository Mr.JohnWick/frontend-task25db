import React from 'react'
import Step1 from './step1'

import axios from 'axios';


class MainForm extends React.Component{

    state = {
      currentStep: 1,
      firstName:'',
      lastName: '',
      dob: ''
    }

    handleChange= input => event =>{
      this.setState({[input]: event.target.value})
    }

    handleSubmit = (event) =>{
        event.preventDefault();
        const person = {
            firstName: this.state.firstName,
            lastName: this.state.lastName,
            dob: this.state.dob.toString()};
        window.console.log("PERSON",person);
        window.console.log("StATE",this.state);
        axios.post('https://shrouded-cliffs-94728.herokuapp.com/person/',person).then(res =>{
            alert("PERSON ADDED")
        })
    }

    render(){
        const {firstName, lastName, dob} = this.state
        const values = {firstName, lastName, dob}

        return(
            <div className="container">
                <h1>Registry Form</h1>
                <h1>Bartas, Pål & Salman</h1>
                <p>Step {this.state.currentStep}</p>

                <form onSubmit={this.handleSubmit}>
                    <Step1 
                    currentStep={this.state.currentStep} 
                    handleChange={this.handleChange}
                    values = {values}
                    ></Step1>                    
                </form>
                <div className="row">
                  <ul>
                    <li><a href="https://shrouded-cliffs-94728.herokuapp.com/person/">Person-endpoint</a></li>
                    <li><a href="https://shrouded-cliffs-94728.herokuapp.com/relationship/">Relationship-endpoint</a></li>
                    <li><a href="https://shrouded-cliffs-94728.herokuapp.com/address/">Address-endpoint</a></li>
                    <li><a href="https://shrouded-cliffs-94728.herokuapp.com/email/">Email-endpoint</a></li>
                    <li> <a href="https://shrouded-cliffs-94728.herokuapp.com/number/">Number-endpoint</a></li>
                  </ul>
                  
                </div>
            </div>

        )
    }
          // The "next" and "previous" button functions
get previousButton(){
    let currentStep = this.state.currentStep;
    // If the current step is not 1, then render the "previous" button
    if(currentStep !==1){
      return (
        <button 
          className="btn btn-secondary" 
          type="button" onClick={this._prev}>
        Previous
        </button>
      )
    }
    // ...else return nothing
    return null;
  }
  
  get nextButton(){
    let currentStep = this.state.currentStep;
    // If the current step is not 3, then render the "next" button
    if(currentStep <3){
      return (
        <button 
          className="btn btn-primary float-right" 
          type="button" onClick={this._next}>
        Next
        </button>        
      )
    }
    // ...else render nothing
    return null;
  }

  _next() {
    let currentStep = this.state.currentStep
    // If the current step is 1 or 2, then add one on "next" button click
    currentStep = currentStep >= 2? 3: currentStep + 1
    this.setState({
      currentStep: currentStep
    })
  }
    
  _prev() {
    let currentStep = this.state.currentStep
    // If the current step is 2 or 3, then subtract one on "previous" button click
    currentStep = currentStep <= 1? 1: currentStep - 1
    this.setState({
      currentStep: currentStep
    })
  }
}

export default MainForm; 