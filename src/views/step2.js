import React from 'react'

class Step2 extends React.Component{

    render(){
       

         if(this.props.currentStep !== 2){
            return null; 
        }
        
        return(
            <div className="row">
                <div className="form-group col-6">
                    <label htmlFor="Relatee">Relatee</label>
                    <input 
                    type="text" 
                        className="form-control" 
                        id="firstName" 
                        name="firstName"
                        placeholder="Enter Relatee" 
                        value={this.props.firstName} 
                        onChange={this.props.handleChange('firstName')}/>
                    <small id="firstNameHelp" className="form-text text-muted">We'll always share your details with the highest bidder.</small>
                </div>
                <button className="btn btn-success btn-block">Sign up</button>
            </div>
        )
    }
}

export default Step2;