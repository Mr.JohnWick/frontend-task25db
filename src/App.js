import React from 'react';
import Main from './views/MainForm'
import './App.css';


function App() {
  return (
    <div className="container-fluid" style={{"textAlign":"center"}}>
      <Main></Main>
    </div>
  );
}

export default App;
